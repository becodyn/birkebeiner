# Small rodent monitoring at Birkebeiner road, Norway

![plot](Figures/Figure0_JohannesSachs.png)

Picture from the high elevation parts of Birkebeiner road 2022. Photo credit: Johannes Sachs.

**Authors:** Magne Neby, Harry Andreassen, Cyril Pierre Milleret, Simen Pedersen, Ana-Maria Peris Tamayo, David Carricondo Sánchez, Erik Versluijs, Barbara Zimmerman.

**Journal:** TBA

**Year:** 2023

**DOI:** TBA

**Data DOI:** TBA

### License

Creative Commons Attribution 4.0 which permits use, sharing, adaptation, distribution, and reproduction in any medium or format as long as the correct citation and a link to original source are provided.

### Description

In 2011, we started a student-based research activity to monitor variation of small rodent density along an elevation gradient following the Birkebeiner Road, in south east Norway. We provide a dataset of small rodent observations that show fluctuating population dynamics across an elevation gradient (300 m to 1,100 m a.s.l) and in contrasting habitats. This dataset encompasses three peaks of the typical 3-4-year vole population cycles; the number of small rodents and shrews captured show synchrony and peaked in year 2014, 2017, 2021. The bank vole Myodes glareolus was by far (87%) the most common species trapped, but also other species were observed (including non-rodent shrews). We also provide digital data collection forms. Please see details and more information in (link to publication).

![plot](Figures/Figure1.png)

The map is showing the study area with the trapping stations (red circles) that each have 10 small rodent traps.

## Getting started with the data

Please download and follow the [user guide](/UserGuide.Rmd) for a step-by-step overview of the data and quick summary statistics such as an index of animal density over time.

![plot](Figures/Figure2.png)

The figure above shows the timeseries on small mammal density index and bank vole density index from Birkebeiner road.

If you want to get in touch, suggest changes or have suggestions for improvement, please contact Magne Neby at magne.neby(at)inn.no.

## Authors and acknowledgment

BZ, HPA and SP conceived the study and designed the scientific protocol; all authors lead the student-oriented field work, lab work, and were involved in data management processes (each in different years); MN wrote the original manuscript, prepared figures and performed data curation; all authors contributed to the final manuscript.

We are grateful to all students for their help and commitment with field and lab work throughout the years and Professor Morten Odden for his part in organising field work in 2022. Thanks to Robert Mesibov for helpful comments on data formatting.

Most of all, we are grateful to Professor Harry P. Andreassen (1962-2019) for his kindness, mentorship and for spreading joy for science.

## Project status

Ongoing, and part of INN's course portfolio. Data will be uploaded annually after end of trapping.
